// specs/start.js
const fs = require('fs');

const testFile = process.argv[2];

if (fs.existsSync(testFile)) {
  require(testFile);
} else {
  console.error(`Test file ${testFile} does not exist.`);
  process.exit(1);
}
