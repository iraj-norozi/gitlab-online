// specs/db-postgres.spec.js
const { Client } = require('pg');

const client = new Client({
  host: 'postgres',
  user: 'postgres',
  password: 'postgres',
  database: 'postgres'
});

client.connect()
  .then(() => console.log("Connected to PostgreSQL"))
  .then(() => client.query('SELECT NOW()'))
  .then(res => console.log(res.rows[0]))
  .then(() => client.end())
  .catch(err => console.error("PostgreSQL error", err))
  .finally(() => process.exit(0));
