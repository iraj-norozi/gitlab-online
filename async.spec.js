// specs/async.spec.js
console.log("Running async tests...");

setTimeout(() => {
  console.log("Async test passed!");
  process.exit(0);
}, 1000);
